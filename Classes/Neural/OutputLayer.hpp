#ifndef TEXT_OUTPUTLAYER_HPP
#define TEXT_OUTPUTLAYER_HPP

#include <Layer.hpp>

class OutputLayer : public Layer {
public:
    OutputLayer(unsigned neuronsAmount, unsigned previousLayerNeuronsAmount);
    virtual ~OutputLayer();
    virtual void update(const Layer &previousLayer) override;
    void calculateWeightsDelta(const Layer &previousLayer, unsigned correctOutput, double learningSpeed);

private:

};

#endif //TEXT_OUTPUTLAYER_HPP
