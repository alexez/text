#include "OutputLayer.hpp"

OutputLayer::OutputLayer(unsigned neuronsAmount, unsigned previousLayerNeuronsAmount) : Layer(neuronsAmount) {
    for (Neuron &neuron : _neurons) {
        neuron.setInitialWeights(previousLayerNeuronsAmount);
    }
}

OutputLayer::~OutputLayer() {

}

void OutputLayer::update(const Layer &previousLayer) {
    for (unsigned i = 0; i < _neurons.size(); ++i) {
        _neurons[i].updateValue(previousLayer);
    }
}

void OutputLayer::calculateWeightsDelta(const Layer &previousLayer, unsigned correctOutput, double learningSpeed) {
    _neurons[correctOutput].calculateWeightsDelta(previousLayer, 1.0, learningSpeed);
}
