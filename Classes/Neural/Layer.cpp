#include "Layer.hpp"

Layer::Layer(unsigned neuronsAmount) {
    for (unsigned i = 0; i < neuronsAmount; ++i) {
        _neurons.push_back(Neuron(i));
    }
}

Layer::~Layer() {

}

unsigned Layer::getSize() const {
    return _neurons.size();
}

void Layer::updateWeights() {
    for (unsigned i = 0; i < _neurons.size(); ++i) {
        _neurons[i].updateWeights();
    }
}

Neuron &Layer::operator[](unsigned index) {
    return _neurons[index];
}

const Neuron &Layer::operator[](unsigned index) const {
    return _neurons[index];
}
