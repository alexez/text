#include "InnerLayer.hpp"

#include <ThreadPool.hpp>

InnerLayer::InnerLayer(unsigned neuronsAmount, unsigned previousLayerNeuronsAmount) : Layer(neuronsAmount + 1) {
    for (unsigned i = 0; i < neuronsAmount; ++i) {
        _neurons[i].setInitialWeights(previousLayerNeuronsAmount);
    }
    _neurons[neuronsAmount].setValue(1.0);
}

InnerLayer::~InnerLayer() {

}

void InnerLayer::update(const Layer &previousLayer) {
    for (unsigned i = 0; i < _neurons.size() - 1; ++i) {
        _neurons[i].updateValue(previousLayer);
    }
}

void InnerLayer::calculateWeightsDelta(const Layer &previousLayer, const Layer &nextLayer, double learningSpeed) {
    for (unsigned i = 0; i < _neurons.size() - 1; ++i) {
        _neurons[i].calculateWeightsDelta(previousLayer, nextLayer, learningSpeed);
    }
}

