#include "NeuralNetwork.hpp"

#include <iostream>
#include <TrainingManager.hpp>
#include <ThreadPool.hpp>
#include <cmath>

NeuralNetwork::NeuralNetwork() {

}

NeuralNetwork::~NeuralNetwork() {

}

void NeuralNetwork::initialize(std::vector<unsigned> innerLayersSizes) {
    _inputLayer = std::make_unique<InputLayer>(LETTER_BLOCKS_NUMBER);
    if (innerLayersSizes.size() > 0) {
        _innerLayers.emplace_back(std::make_unique<InnerLayer>(innerLayersSizes[0], _inputLayer->getSize()));
        for (unsigned i = 1; i < innerLayersSizes.size(); ++i) {
            _innerLayers.emplace_back(std::make_unique<InnerLayer>(innerLayersSizes[i], _innerLayers[i - 1]->getSize()));
        }
    }
    if (innerLayersSizes.size() > 0) {
        _outputLayer = std::make_unique<OutputLayer>(1, _innerLayers[_innerLayers.size() - 1]->getSize());
    } else {
        _outputLayer = std::make_unique<OutputLayer>(1, _inputLayer->getSize());
    }
}

void NeuralNetwork::train(const std::array<double, LETTER_BLOCKS_NUMBER> &initialValues, unsigned result, double learningSpeed) {
    _inputLayer->setValues(initialValues);
    updateLayers();

    if (_innerLayers.size() == 0) {
        _outputLayer->calculateWeightsDelta(*_inputLayer, 0, learningSpeed);
    } else if (_innerLayers.size() == 1) {
        _outputLayer->calculateWeightsDelta(*_innerLayers[_innerLayers.size() - 1], 0, learningSpeed);
        _innerLayers[0]->calculateWeightsDelta(*_inputLayer, *_outputLayer, learningSpeed);
//        ThreadPool::getInstance().wait();
    } else {
        _outputLayer->calculateWeightsDelta(*_innerLayers[_innerLayers.size() - 1], 0, learningSpeed);
        _innerLayers[_innerLayers.size() - 1]->calculateWeightsDelta(*_innerLayers[_innerLayers.size() - 2], *_outputLayer, learningSpeed);
//        ThreadPool::getInstance().wait();
        for (unsigned i = _innerLayers.size() - 2; i >= 1; --i) {
            _innerLayers[i]->calculateWeightsDelta(*_innerLayers[i - 1], *_innerLayers[i + 1], learningSpeed);
//            ThreadPool::getInstance().wait();
        }
        _innerLayers[0]->calculateWeightsDelta(*_inputLayer, *_innerLayers[1], learningSpeed);
//        ThreadPool::getInstance().wait();
    }

    _outputLayer->updateWeights();
    for (unsigned i = 0; i < _innerLayers.size(); ++i) {
        _innerLayers[i]->updateWeights();
    }
}

void NeuralNetwork::updateLayers() const {
    if (_innerLayers.size() != 0) {
        _innerLayers[0]->update(*_inputLayer);
        for (unsigned i = 1; i < _innerLayers.size(); ++i) {
            _innerLayers[i]->update(*_innerLayers[i - 1]);
        }
        _outputLayer->update(*_innerLayers[_innerLayers.size() - 1]);
    } else {
        _outputLayer->update(*_inputLayer);
    }
}

double NeuralNetwork::getMSE(unsigned correctIndex) const {
    return pow(1.0 - _outputLayer->operator[](correctIndex).getValue(), 2.0);
}

double NeuralNetwork::getResult(const std::array<double, LETTER_BLOCKS_NUMBER> &initialValues) const {
    _inputLayer->setValues(initialValues);
    updateLayers();

    const OutputLayer &outputLayer = *_outputLayer;
    return outputLayer[0].getValue();
}
