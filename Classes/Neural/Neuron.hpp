#ifndef TEXT_NEURON_HPP
#define TEXT_NEURON_HPP

#include <vector>
#include <Layer.hpp>

class Layer;

class Neuron {
public:
    Neuron(unsigned index);
    ~Neuron();
    void updateValue(const Layer &previousLayer);
    double getValue() const;
    double getError() const;
    double getWeight(unsigned index) const;
    void setInitialWeights(unsigned weightsAmount);
    void setValue(double value);
    void calculateWeightsDelta(const Layer &previousLayer, double correctValue, double learningSpeed);
    void calculateWeightsDelta(const Layer &previousLayer, const Layer &nextLayer, double learningSpeed);
    void updateWeights();

private:
    unsigned _index;
    double _error;
    double _outputValue;
    double _inputValue;
    std::vector<double> _weights;
    std::vector<double> _weightsDelta;
};

#endif //TEXT_NEURON_HPP
