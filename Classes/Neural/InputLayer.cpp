#include "InputLayer.hpp"

InputLayer::InputLayer(unsigned neuronsAmount) : Layer(neuronsAmount + 1) {
    _neurons[neuronsAmount].setValue(1.0);
}

InputLayer::~InputLayer() {

}

void InputLayer::update(const Layer &previousLayer) {

}

void InputLayer::setValues(const std::array<double, LETTER_BLOCKS_NUMBER> &initialValues) {
    for (unsigned i = 0; i < _neurons.size() - 1; ++i) {
        _neurons[i].setValue(initialValues[i]);
    }
}
