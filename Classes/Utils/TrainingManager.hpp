#ifndef TEXT_DICTIONARY_HPP
#define TEXT_DICTIONARY_HPP

#include <vector>
#include <map>
#include <Defines.hpp>

class TrainingLetter {
public:
    TrainingLetter();
    TrainingLetter(const std::string letterPath);
    ~TrainingLetter();
    const std::array<double, LETTER_BLOCKS_NUMBER> &getValues() const;

private:
    std::array<double, LETTER_BLOCKS_NUMBER> _values;
};

class TrainingSet {
public:
    TrainingSet(const std::string &setPath, const std::string &name);
    ~TrainingSet();
    const TrainingLetter &getLetter(unsigned index) const;
    const std::string &getName() const;

private:
    std::array<TrainingLetter, SET_SIZE> _letters;
    std::string _name;
};

class TrainingManager {
public:
    static TrainingManager &getInstance();
    TrainingManager(TrainingManager const &) = delete;
    TrainingManager &operator=(TrainingManager const &) = delete;

    void addSet(const std::string &setName);
    const TrainingSet &getSet(unsigned index) const;
    unsigned getSetsSize() const;

private:
    TrainingManager();

    std::vector<TrainingSet> _sets;
};

#endif //TEXT_DICTIONARY_HPP
