#ifndef TEXT_THREAD_POOL_HPP
#define TEXT_THREAD_POOL_HPP

#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <future>
#include <functional>
#include <stdexcept>

class ThreadPool {
public:
    static ThreadPool &getInstance() {
        static ThreadPool instance(std::thread::hardware_concurrency() * 2);
        return instance;
    }

    template<class F, class... Args>
    auto enqueue(F &&f, Args &&... args) -> std::future<typename std::result_of<F(Args...)>::type>;
    void wait();
    ~ThreadPool();
private:
    ThreadPool(unsigned threadsNumber);

    std::vector<std::thread> _workers;
    unsigned _threadsNumber;
    std::queue<std::function<void()>> _tasks;

    std::mutex _queueMutex;
    std::condition_variable _condition;
    std::atomic<bool> _isStopped;
    unsigned short _added;
    std::atomic<unsigned short> _processed;
};

inline ThreadPool::ThreadPool(unsigned threadsNumber)
        : _isStopped(false), _threadsNumber(threadsNumber), _processed(0), _added(0) {
    for (unsigned i = 0; i < _threadsNumber; ++i) {
        _workers.emplace_back(
                [this, i] {
                    for (;;) {
                        std::function<void()> task;
                        {
                            std::unique_lock<std::mutex> lock(_queueMutex);
                            _condition.wait(lock, [this] { return _isStopped || !_tasks.empty(); });
                            if (_isStopped && _tasks.empty()) {
                                return;
                            }
                            task = std::move(_tasks.front());
                            _tasks.pop();
                        }
                        task();
                        ++_processed;
                    }
                }
        );
    }
}

template<class F, class... Args>
auto ThreadPool::enqueue(F &&f, Args &&... args) -> std::future<typename std::result_of<F(Args...)>::type> {
    using return_type = typename std::result_of<F(Args...)>::type;

    auto task = std::make_shared<std::packaged_task<return_type()>>(std::bind(std::forward<F>(f), std::forward<Args>(args)...));
    std::future<return_type> res = task->get_future();
    if (_isStopped) {
        throw std::runtime_error("enqueue on stopped ThreadPool");
    }
    {
        std::unique_lock<std::mutex> lock(_queueMutex);
        _tasks.emplace([task]() { (*task)(); });
    }
    ++_added;
    _condition.notify_one();
    return res;
}

inline void ThreadPool::wait() {
    _condition.notify_all();
    while (_added != _processed) { }
    _added = 0;
    _processed = 0;
}

inline ThreadPool::~ThreadPool() {
    {
        std::unique_lock<std::mutex> lock(_queueMutex);
        _isStopped = true;
    }
    _condition.notify_all();

    for (std::thread &worker: _workers)
        worker.join();
}

#endif //TEXT_THREAD_POOL_HPP
