#ifndef TEXT_WORD_HPP
#define TEXT_WORD_HPP

#include <vector>
#include <Letter.hpp>

class Word {
public:
    Word(const Image &source, const Rect &rect);
    ~Word();

    void process();
    const std::vector<Letter> &getLetters() const;

private:
    void clipRect();
    std::vector<Rect> getSpaces() const;

    const Image &_source;
    Rect _rect;
    std::vector<Letter> _letters;
};

#endif //TEXT_WORD_HPP
