#include "Word.hpp"

#include <iostream>

Word::Word(const Image &source, const Rect &rect)
        : _source(source), _rect(rect) {

}

Word::~Word() {

}

void Word::process() {
    clipRect();
    const std::vector<Rect> &spaces = getSpaces();
    unsigned start = _rect.left;
    for (const auto &space : spaces) {
        _letters.emplace_back(Letter(_source, {_rect.top, _rect.bottom, start, space.left - 1}));
        start = space.right + 1;
    }
    _letters.emplace_back(Letter(_source, {_rect.top, _rect.bottom, start, _rect.right}));
//    std::cout << "letters = " << _letters.size() << std::endl;

    for (Letter &letter : _letters) {
        letter.process();
    }
}

const std::vector<Letter> &Word::getLetters() const {
    return _letters;
}

void Word::clipRect() {
    for (unsigned i = _rect.top; i <= _rect.bottom; ++i) {
        auto value = _source.getLineHorizontal(i, _rect);
        if (value < 1.0) {
            _rect.top = i;
            break;
        }
    }
    for (unsigned i = _rect.bottom; i >= _rect.top; --i) {
        auto value = _source.getLineHorizontal(i, _rect);
        if (value < 1.0) {
            _rect.bottom = i;
            break;
        }
    }
//    std::cout << "rect.top = " << _rect.top << std::endl;
//    std::cout << "rect.bottom = " << _rect.bottom << std::endl;
//    std::cout << "rect.left = " << _rect.left << std::endl;
//    std::cout << "rect.right = " << _rect.right << std::endl;
}

std::vector<Rect> Word::getSpaces() const {
    std::vector<Rect> spaces;
    bool inSpace = false;
    unsigned start = _rect.left;
    for (unsigned i = _rect.left; i <= _rect.right; ++i) {
        auto value = _source.getLineVertical(i, _rect);
        if (value >= 1.0 && !inSpace) {
            start = i;
            inSpace = true;
        } else if (value < 1.0 && inSpace) {
            spaces.emplace_back(Rect{0, 0, start, i - 1});
            inSpace = false;
        }
    }
    return spaces;
}
