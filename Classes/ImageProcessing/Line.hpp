#ifndef TEXT_LINE_HPP
#define TEXT_LINE_HPP

#include <Word.hpp>
#include <Rect.hpp>

class Line {
public:
    Line(const Image &source, const Rect &rect);
    ~Line();

    void process();
    const std::vector<Word> &getWords() const;

private:
    void clipRect();
    std::vector<Rect> getSpaces() const;

    const Image &_source;
    Rect _rect;
    std::vector<Word> _words;
};

#endif //TEXT_LINE_HPP
