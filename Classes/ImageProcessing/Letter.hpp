#ifndef TEXT_LETTER_HPP
#define TEXT_LETTER_HPP

#include <array>
#include <Image.hpp>
#include <Rect.hpp>
#include <Defines.hpp>

class Letter {
public:
    Letter(const Image &source, const Rect &rect);

    void process();
    const std::array<double, LETTER_BLOCKS_NUMBER> &getPieces() const;

private:
    void clipRect();

    const Image &_source;
    Rect _rect;
    std::array<double, LETTER_BLOCKS_NUMBER> _pieces;
};

#endif //TEXT_LETTER_HPP
